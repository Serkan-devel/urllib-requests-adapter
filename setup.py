from distutils.core import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='urllib-requests-adapter',
    version='0.0.1',
    author='Matrixcoffee',
    author_email='Matrixcoffee@users.noreply.github.com',
    packages=['requests'],
    url='https://gitlab.com/Matrixcoffee/urllib-requests-adapter/',
    license='LICENSE',
    description='Make simple requests-using programs use urllib instead, without changing a line of code!',
    long_description=long_description,
    long_description_content_type="text/markdown",
)

